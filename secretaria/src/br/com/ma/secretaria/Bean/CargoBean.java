package br.com.ma.secretaria.Bean;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.ma.secretaria.tipocargoEnum.TipoCargoEnum;
import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.cargo.Cargo;
import br.com.secretaria.cargo.CargoFacadeBean;

@ManagedBean
@ViewScoped
public class CargoBean {

	@EJB
	private CargoFacadeBean cargofacadeBean;

	private Cargo cargoCadastro;
	private List<Cargo> cargos;
	private List<TipoCargoEnum> tipoCargos;

	private boolean btnSalvar;

	private boolean btnEditar;

	@PostConstruct
	public void init() {
		cargos = cargofacadeBean.findAll();
		TipoCargoEnum[] vetor = TipoCargoEnum.values();
		tipoCargos = Arrays.asList(vetor);
		novo();
	}

	public void novo() {
		cargoCadastro = new Cargo();
		salvarDados();
	}

	public void salvar() {
		try {
			cargofacadeBean.salvar(cargoCadastro);
			cargos = cargofacadeBean.findAll();
			FacesUtil.addMsgInfor("Cargo " + cargoCadastro.getDescricao() + " salvo com sucesso!");
			novo();

		} catch (

		Exception e) {
			FacesUtil.addMsgERROR("Erro ao salvar Cargo :" + e.getMessage());
		}
	}

	public void excluir(ActionEvent e) {
		try {
			Cargo cargo = (Cargo) e.getComponent().getAttributes().get("CargoSelecionado");
			cargofacadeBean.excluir(cargo.getCargoId());
			cargos = cargofacadeBean.findAll();
			FacesUtil.addMsgInfor("Unidade " + cargo.getDescricao() + " removido com sucesso!");
		} catch (Exception ex) {
			FacesUtil.addMsgERROR("Erro ao excluir Cargo :" + ex.getMessage());
		}

	}

	public void selecaoCargo(ActionEvent e) {
		this.cargoCadastro = (Cargo) e.getComponent().getAttributes().get("CargoSelecionado");
		editarDados();
	}

	public void editar() {
		try {
			cargofacadeBean.alterar(cargoCadastro);
			FacesUtil.addMsgInfor("Edi��o realizada com sucesso!");

		} catch (Exception ex) {
			ex.printStackTrace();
			FacesUtil.addMsgERROR("Erro ao editar Titularidade :" + ex.getMessage());
		}
	}
	
	public void salvarDados() {
		this.btnSalvar = true;
		this.btnEditar = false;
	}
	
	public void editarDados() {
		this.btnSalvar = false;
		this.btnEditar = true;
	}

	
	
	
	
	public boolean isBtnSalvar() {
		return btnSalvar;
	}

	public void setBtnSalvar(boolean btnSalvar) {
		this.btnSalvar = btnSalvar;
	}

	public boolean isBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(boolean btnEditar) {
		this.btnEditar = btnEditar;
	}

	public List<TipoCargoEnum> getTipoCargos() {
		return tipoCargos;
	}

	public void setTipoCargos(List<TipoCargoEnum> tipoCargos) {
		this.tipoCargos = tipoCargos;
	}

	public Cargo getCargoCadastro() {
		return cargoCadastro;
	}

	public void setCargoCadastro(Cargo cargoCadastro) {
		this.cargoCadastro = cargoCadastro;
	}

	public List<Cargo> getCargos() {
		return cargos;
	}

	public void setCargos(List<Cargo> cargos) {
		this.cargos = cargos;
	}

}
