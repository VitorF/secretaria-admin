package br.com.ma.secretaria.Bean;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.disciplina.DisciplinaFacadeBean;
import br.com.secretaria.tiponivelEnum.TipoNivelEnum;

@ManagedBean
@ViewScoped
public class DisciplinaBean {
	
	@EJB
	private DisciplinaFacadeBean disciplinaFacadeBean;
	
	private List<Disciplina> disciplinas;
	
	private List<TipoNivelEnum> tipoNivelEnums;
	
    private Disciplina disciplinaSelecionada;
    
    private boolean btnEditar;
    
    @PostConstruct
    public void init() {
    	tipoNivelEnums =  Arrays.asList(TipoNivelEnum.values());
    	disciplinaSelecionada =  new Disciplina();
    	reload();
    }
    
    
    
    public void novo() {
    	disciplinaSelecionada =  new Disciplina();
    	btnEditar =false;
    }
    
    
    public void salvar() {
    	
    	try {
    		validacao();
    		
    		disciplinaFacadeBean.update(disciplinaSelecionada);
    		novo();
    		reload();
    		System.out.println(btnEditar);
    		if(btnEditar) {
        		FacesUtil.addMsgInfor("Disciplina Atualizada com sucesso !");

    		}else {
        		FacesUtil.addMsgInfor("Disciplina Salva  com sucesso !");

    		}
    		
    	}catch(Exception e) {
    		FacesUtil.addMsgWARN(e.getMessage());
    	}
    	
    }
    
    
    
    
    public void remover(ActionEvent e){
    	
    	Disciplina  disciplina   = (Disciplina) e.getComponent().getAttributes().get("disciplinaSelecionada");
    
    	disciplinaFacadeBean.remover(disciplina.getDisciplinaId());
    	
    	reload();
    	
    	FacesUtil.addMsgInfor("disciplina removida com sucesso");
    }
    
    
    
    public void editar(ActionEvent e) {
     this.disciplinaSelecionada = (Disciplina) e.getComponent().getAttributes().get("disciplinaSelecionada");
     btnEditar =  true;
		System.out.println(btnEditar);

    }
    
    
    
    
    
    
    private void validacao() {
    	if(disciplinaSelecionada.getDescricao() == null ) {
    		throw new RuntimeException("Informe a descri��o da disciplina");
    	}
    	
    	if(disciplinaSelecionada.getTipoNivelEnum() == null){
    		throw new RuntimeException(" Informe o  Nivel da disciplina");
    	}
    	
    	if(disciplinaSelecionada.getCargaHoraria() == null) {
    		throw new RuntimeException(" Informe a carga horaria da disciplina");
    	}
    }
    
    
    private void reload() {
		disciplinas = disciplinaFacadeBean.findAll();

    }



	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}



	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}



	public List<TipoNivelEnum> getTipoNivelEnums() {
		return tipoNivelEnums;
	}



	public void setTipoNivelEnums(List<TipoNivelEnum> tipoNivelEnums) {
		this.tipoNivelEnums = tipoNivelEnums;
	}



	public Disciplina getDisciplinaSelecionada() {
		return disciplinaSelecionada;
	}



	public void setDisciplinaSelecionada(Disciplina disciplinaSelecionada) {
		this.disciplinaSelecionada = disciplinaSelecionada;
	}
    
    
    public boolean isBtnEditar() {
		return btnEditar;
	}
    
    
    

}
