package br.com.ma.secretaria.Bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.estado.Estado;
import br.com.secretaria.estado.EstadoFacadeBean;
import br.com.secretaria.municipio.Municipio;
import br.com.secretaria.municipio.MunicipioFacadeBean;
import br.com.secretaria.tiposangeEnum.TipoSangeEnum;

@ManagedBean
@ViewScoped
public class FuncionarioBean {
	
	@EJB
	private FuncionarioFacabeBean funcionarioFacabeBean;
	@EJB
	private EstadoFacadeBean estadoFacadeBean;
	@EJB
	private MunicipioFacadeBean municipioFacadeBean;

	private Funcionario funcionarioCadastro;
	private List<TipoSangeEnum> tiposSangue;
	private List<Estado> estados;
	private List<Municipio> municipios;
	private List<Funcionario> funcionarios;
	private List<Funcionario> funcionariosFilter;
	private Estado estadoSelecionado;
	private boolean btnSalvar;
	private boolean btnAtualizar;

	@PostConstruct
	public void init() {
		TipoSangeEnum[] vetor = TipoSangeEnum.values();
		tiposSangue = Arrays.asList(vetor);
		estados = estadoFacadeBean.findAll();
		if(FacesUtil.getParam("funcionarioId") == null) {
		funcionarios = funcionarioFacabeBean.findAll();
		funcionarioCadastro = new Funcionario();
		estadoSelecionado = new Estado();
		municipios = new ArrayList<>();
		salvarVisible();
		}else {
			loadEdit(Long.parseLong(FacesUtil.getParam("funcionarioId")));
		}
		
		}

	public void buscaMunicipios() {

		municipios = municipioFacadeBean.buscaMunicipioPorEstado(estadoSelecionado);

	}

	public String salvar() {
		try {
			System.out.println(funcionarioCadastro);
			funcionarioFacabeBean.salvar(funcionarioCadastro);
			Messages.addGlobalInfo("Funcionario " + funcionarioCadastro.getNome() + " Cadastrado com Sucesso", null);
			funcionarioCadastro = new Funcionario();
			return "funcionario";
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalError("Erro ao Salvar Funcionario : " + e.getMessage(), null);
		 return null;
		}
	}

	public void excluir(ActionEvent e) {
		try {
			Funcionario funcionario = (Funcionario) e.getComponent().getAttributes().get("funcionarioSelecionado");
			funcionarioFacabeBean.excluir(funcionario.getFuncionarioId());
			funcionarios = funcionarioFacabeBean.findAll();
			Messages.addGlobalInfo("Funcionario " + funcionario.getNome() + " excluido com sucesso !", null);
		} catch (Exception ex) {
			Messages.addGlobalError("Erro ao Excluir Funcionario : " + ex.getMessage(), null);
		}
	}
	
	public String editar() {
		try {
			funcionarioFacabeBean.alterar(funcionarioCadastro);
			Messages.addGlobalInfo("Dados atualizados com sucesso", null);
			return "funcionario";
		}catch(Exception ex) {
			ex.printStackTrace();
			Messages.addGlobalError("Erro ao Atualizar Cadastro do Funcionario : "+ex.getMessage(), null);
		  return null;
		}
	}
	
	private void loadEdit(Long funcionarioId) {
		funcionarioCadastro = funcionarioFacabeBean.findByPrimaryKey(funcionarioId);
		estadoSelecionado = funcionarioCadastro.getMunicipio().getEstado();
		municipios = municipioFacadeBean.buscaMunicipioPorEstado(estadoSelecionado);
		editarVisible();
	}
	
	private void salvarVisible() {
		this.btnSalvar =  true;
		this.btnAtualizar = false;
	}
	
	private void editarVisible() {
		this.btnSalvar = false;
		this.btnAtualizar = true;
	}
	
	
	

	public boolean isBtnSalvar() {
		return btnSalvar;
	}

	public void setBtnSalvar(boolean btnSalvar) {
		this.btnSalvar = btnSalvar;
	}

	public boolean isBtnAtualizar() {
		return btnAtualizar;
	}

	public void setBtnAtualizar(boolean btnAtualizar) {
		this.btnAtualizar = btnAtualizar;
	}

	public Funcionario getFuncionarioCadastro() {
		return funcionarioCadastro;
	}

	public void setFuncionarioCadastro(Funcionario funcionarioCadastro) {
		this.funcionarioCadastro = funcionarioCadastro;
	}

	public List<TipoSangeEnum> getTiposSangue() {
		return tiposSangue;
	}

	public void setTiposSangue(List<TipoSangeEnum> tiposSangue) {
		this.tiposSangue = tiposSangue;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {

		this.estados = estados;
	}

	public Estado getEstadoSelecionado() {

		return estadoSelecionado;
	}

	public void setEstadoSelecionado(Estado estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}

	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public List<Funcionario> getFuncionariosFilter() {
		return funcionariosFilter;
	}

	public void setFuncionariosFilter(List<Funcionario> funcionariosFilter) {
		this.funcionariosFilter = funcionariosFilter;
	}

}
