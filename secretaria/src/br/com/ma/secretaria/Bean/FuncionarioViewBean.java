package br.com.ma.secretaria.Bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.ma.secretaria.util.FTPUtil;
import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.arquivofuncionario.ArquivoFuncionario;
import br.com.secretaria.arquivofuncionario.ArquivoFuncionarioFacadeBean;
import br.com.secretaria.funcionariotitularidae.FuncionarioTitularidade;
import br.com.secretaria.funcionariotitularidae.FuncionarioTitularidadeFacadeBean;
import br.com.secretaria.titularidade.Titularidade;
import br.com.secretaria.titularidade.TitularidadeFacadeBean;

@ViewScoped
@ManagedBean
public class FuncionarioViewBean {

	@EJB
	private FuncionarioFacabeBean funcionarioFacabeBean;
	@EJB
	private TitularidadeFacadeBean titularidadeFacadeBean;
	@EJB
	private ArquivoFuncionarioFacadeBean arquivoFuncionarioFacadeBean;

	@EJB
	private FuncionarioTitularidadeFacadeBean funcionarioTitularidadeFacadeBean;

	private List<Titularidade> titularidades;
	private Funcionario funcionario;
	private List<ArquivoFuncionario> arquivos;
	private List<FuncionarioTitularidade> funcionariosTitutularidades;
	private Titularidade titularidadeSelecionada;
	private FTPUtil ftpUtil ;
	private StreamedContent file;

	@PostConstruct
	public void init() {
		titularidades = titularidadeFacadeBean.findAll();
		String funcionarioId = FacesUtil.getParam("funcionarioId");
		funcionario = funcionarioFacabeBean.findByPrimaryKey(Long.valueOf(funcionarioId));
		funcionariosTitutularidades = funcionarioTitularidadeFacadeBean.buscaTitularidadesPorFuncionario(funcionario);
		arquivos = arquivoFuncionarioFacadeBean.buscaArquivoPorFuncionario(funcionario);
		 ftpUtil =  new FTPUtil();
	}

	public void salvarTitularidadeFunc() {
		try {
			if (titularidadeSelecionada != null && titularidadeSelecionada.getTitularidadeId() != null) {
				FuncionarioTitularidade funcionarioTitularidade = new FuncionarioTitularidade();
				funcionarioTitularidade.setFuncionario(funcionario);
				funcionarioTitularidade.setTitularidade(titularidadeSelecionada);
				funcionarioTitularidadeFacadeBean.salvar(funcionarioTitularidade);
				titularidadeSelecionada = new Titularidade();
				funcionariosTitutularidades = funcionarioTitularidadeFacadeBean
						.buscaTitularidadesPorFuncionario(funcionario);
				Messages.addGlobalInfo("Titularidade Adicionada com Sucesso", null);
			}

		} catch (Exception e) {
			Messages.addGlobalError("Erro : " + e.getMessage(), null);
		}
	}

	public void excluirFuncionarioTitularidade(ActionEvent e) {
		try {
			FuncionarioTitularidade funcionarioTitularidade = (FuncionarioTitularidade) e.getComponent().getAttributes()
					.get("titularidadeSelecao");
			funcionarioTitularidadeFacadeBean.excluir(funcionarioTitularidade);
			funcionariosTitutularidades = funcionarioTitularidadeFacadeBean
					.buscaTitularidadesPorFuncionario(funcionario);
			Messages.addGlobalInfo("Titularidade Excluido com Sucesso", null);

		} catch (Exception ex) {
			Messages.addGlobalError("Erro : " + ex.getMessage(), null);

		}
	}

	public void uploadArquivo(FileUploadEvent event) {
		try {
			ArquivoFuncionario arquivoFuncionario =  new ArquivoFuncionario();
			String nome  = new Date().getTime()+""+event.getFile().getFileName();
			InputStream arquivo = event.getFile().getInputstream();
			
			
			arquivoFuncionario.setArquivo(nome);
			arquivoFuncionario.setFuncionario(funcionario);
			arquivoFuncionarioFacadeBean.salvar(arquivoFuncionario);
			arquivos = arquivoFuncionarioFacadeBean.buscaArquivoPorFuncionario(funcionario);
			ftpUtil.upalodArquivo("anexos",arquivo, nome);
			Messages.addFlashGlobalInfo("upload Efetuado com sucesso" , null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Messages.addFlashGlobalError("Erro ao efetua upload : "+e.getMessage() , null);
		}
	}
	
	public void excluirArquivo(ActionEvent e) {
		try {
			
		ArquivoFuncionario arquivo =  (ArquivoFuncionario) e.getComponent().getAttributes().get("arquivoSelecionado");
		ftpUtil.excluirArquivo("anexos",arquivo.getArquivo());
		arquivoFuncionarioFacadeBean.excluir(arquivo);
		arquivos = arquivoFuncionarioFacadeBean.buscaArquivoPorFuncionario(funcionario);
		Messages.addGlobalInfo("Excluido com Sucesso", null);
		}catch(Exception ex) {
			ex.printStackTrace();
		 Messages.addGlobalError("Erro ao Excluir Arquivo : "+ex.getMessage(), null);	
		}
	}
	
	public void downloadArquivo(ActionEvent e) {
		try {
		ArquivoFuncionario arquivo =  (ArquivoFuncionario) e.getComponent().getAttributes().get("arquivoSelecionado");
		InputStream inputStream = ftpUtil.buscaArquivo("anexos",arquivo.getArquivo()); 	
		this.file = new DefaultStreamedContent(inputStream,"", arquivo.getArquivo());
		}catch(Exception ex) {
			 Messages.addGlobalError("Erro ao realizar Download do Arquivo : "+ex.getMessage(), null);
		}
	}


	public List<Titularidade> getTitularidades() {
		return titularidades;
	}

	public void setTitularidades(List<Titularidade> titularidades) {
		this.titularidades = titularidades;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<ArquivoFuncionario> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<ArquivoFuncionario> arquivos) {
		this.arquivos = arquivos;
	}

	public List<FuncionarioTitularidade> getFuncionariosTitutularidades() {
		return funcionariosTitutularidades;
	}

	public void setFuncionariosTitutularidades(List<FuncionarioTitularidade> funcionariosTitutularidades) {
		this.funcionariosTitutularidades = funcionariosTitutularidades;
	}

	public Titularidade getTitularidadeSelecionada() {
		return titularidadeSelecionada;
	}

	public void setTitularidadeSelecionada(Titularidade titularidadeSelecionada) {
		this.titularidadeSelecionada = titularidadeSelecionada;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	

}
