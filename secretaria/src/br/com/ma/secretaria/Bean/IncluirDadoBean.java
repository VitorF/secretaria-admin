package br.com.ma.secretaria.Bean;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import br.com.ma.secretaria.util.EstadoCidades;
import br.com.secretaria.estado.Estado;
import br.com.secretaria.estado.EstadoFacadeBean;
import br.com.secretaria.municipio.Municipio;
import br.com.secretaria.municipio.MunicipioFacadeBean;

@ManagedBean
public class IncluirDadoBean {

	@EJB
	private EstadoFacadeBean estadoFacadeBean;

	@EJB
	private MunicipioFacadeBean municipioFacadeBean;

	public void incluir() {

		List<EstadoCidades> estadosCidades = buscaEstadosCidades();
		Estado estado;
		for (EstadoCidades estadoCidades : estadosCidades) {
			estado = new Estado();
			estado.setDescricao(estadoCidades.getNome());
			estado.setSigla(estadoCidades.getSigla());

			estado = estadoFacadeBean.salvar(estado);
			Municipio municipio;
			for (String cidade : estadoCidades.getCidades()) {
				municipio = new Municipio();
				municipio.setDescricao(cidade);
				municipio.setEstado(estado);
				municipioFacadeBean.salvar(municipio);
			}

		}
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "sucesso", "sucesso");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage("sucesso", msg);
	}

	private List<EstadoCidades> buscaEstadosCidades() {

		Gson gson = new Gson();
		JsonParser json = new JsonParser();
		JsonElement jsonElement;
		try {                        
			jsonElement = json.parse(new BufferedReader(new InputStreamReader(new FileInputStream("C:/Desenvolvimento/estados-cidades.json"), "UTF-8")));
			String jsonString = jsonElement.toString();
			EstadoCidades[] estadosvetor = gson.fromJson(jsonString, EstadoCidades[].class);

			return Arrays.asList(estadosvetor);
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}
}
