package br.com.ma.secretaria.Bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.cargo.Cargo;
import br.com.secretaria.cargo.CargoFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.matricula.MatriculaFacadeBean;
import br.com.secretaria.unidade.Unidade;
import br.com.secretaria.unidade.UnidadeFacadeBean;

@ManagedBean
@ViewScoped
public class MatriculaBean {

	@EJB
	private MatriculaFacadeBean matriculaFacadeBean;
	@EJB
	private FuncionarioFacabeBean funcionarioFacabeBean;
	@EJB
	private CargoFacadeBean cargoFacadeBean;
	@EJB
	private UnidadeFacadeBean unidadeFacadeBean;

	private List<Matricula> matriculas;
	private List<Cargo> cargos;
	private List<Unidade> unidades;
	
	//
	private Funcionario funcionarioSelecionado;
	private Matricula matriculaCadastro;
	private Cargo cargoSelecionado;
	private boolean btnAtualizar;
	private String cpfBusca;

	@PostConstruct
	public void init() {
		if (FacesUtil.getParam("matriculaId") == null) {
			novo();
			matriculas = matriculaFacadeBean.findAll();
			btnAtualizar = false;
		} else {
			pgEdicao();
			btnAtualizar = true;
		}
	}

	public void novo() {
		matriculaCadastro = new Matricula();
		cargos = cargoFacadeBean.findAll();
		unidades = unidadeFacadeBean.findAll();
		funcionarioSelecionado =  new Funcionario();
				
	}

	private void pgEdicao() {
		String matriculaId = FacesUtil.getParam("matriculaId");
		matriculaCadastro = matriculaFacadeBean.findPrimaryKey(Long.parseLong(matriculaId));
		funcionarioSelecionado = matriculaCadastro.getFuncionario();
		this.cpfBusca = funcionarioSelecionado.getCpf();
		cargos = cargoFacadeBean.findAll();
		unidades = unidadeFacadeBean.findAll();
	}
	
	
	/*
	 * Buscando o funcionario com o cpf informado
	 */
	
	public void buscaCpf() {
		if(cpfBusca != null && !cpfBusca.isEmpty()) {
			
			funcionarioSelecionado = funcionarioFacabeBean.buscaPorCpf(cpfBusca);
			
			if( funcionarioSelecionado == null || funcionarioSelecionado.getFuncionarioId() == null) {
				FacesUtil.addMsgWARN("Nenhum funcionario encontrado!!");
			}
			
		}else {
			FacesUtil.addMsgWARN("Informe o cpf do funcionario antes de realizar a busca!!");
		}
	}
	
	/*
	 * Salva a Matricula no banco de Dados  Verificando se o usuario foi selecionado  
	 * caso n�o disparar uma excer��o
	 */
	public void salvar() {
		
		try {
			
			if( funcionarioSelecionado == null || funcionarioSelecionado.getFuncionarioId() == null)  {
				 throw new IllegalArgumentException("Informe o funcionario ");
			}
			matriculaCadastro.setDataCadastro(new Date());
			matriculaCadastro.setFuncionario(funcionarioSelecionado);
			Matricula matricula = matriculaFacadeBean.salvar(matriculaCadastro);
			matricula.setNumeroMatricula(String.valueOf((matricula.getMatriculaId() + Long.parseLong("100"))));
            
			matriculaFacadeBean.alterar(matricula);
			
			novo();
			FacesUtil.addMsgInfor("Matricula Cadastrada com sucesso !!");
			
			
		}catch(IllegalArgumentException e) {
			FacesUtil.addMsgWARN(e.getMessage());
		}catch(Exception e) {
			FacesUtil.addMsgERROR(e.getMessage());
		}
		
		
	}
	
	
	public void  atualizarCadastro() {
		try {
			if( funcionarioSelecionado == null || funcionarioSelecionado.getFuncionarioId() == null)  {
				 throw new IllegalArgumentException("Informe o funcionario ");
			}
			matriculaCadastro.setDataAltercao(new Date());
			matriculaCadastro.setFuncionario(funcionarioSelecionado);
			
		
		matriculaFacadeBean.alterar(matriculaCadastro);
			
	   FacesUtil.addMsgInfor("Matricula Atualizada com sucesso !!");
		
	   FacesContext.getCurrentInstance().getExternalContext().redirect("./matricula.jsf"); ;
			
		}catch(IllegalArgumentException e) {
			FacesUtil.addMsgWARN(e.getMessage());
		}catch(Exception e) {
			FacesUtil.addMsgERROR(e.getMessage());
		}
		
		
	}
	
	
	public void excluirMatricula(ActionEvent e) {
		
		try {
			Matricula matricula = (Matricula) e.getComponent().getAttributes().get("matriculaSelecionado");
			
			matriculaFacadeBean.excluir(matricula.getMatriculaId());
			
			matriculas = matriculaFacadeBean.findAll();
			   FacesUtil.addMsgInfor("Matricula excluida com sucesso !!");

			
		}catch(Exception ex ) {
			FacesUtil.addMsgERROR(ex.getMessage());
		}
		
	}
	

	private void pgNovo() {

	}
	
	

	public boolean isBtnAtualizar() {
		return btnAtualizar;
	}

	public void setBtnAtualizar(boolean btnAtualizar) {
		this.btnAtualizar = btnAtualizar;
	}

	public List<Matricula> getMatriculas() {
		return matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

	public List<Unidade> getUnidades() {
		return unidades;
	}

	public void setUnidades(List<Unidade> unidades) {
		this.unidades = unidades;
	}

	public Funcionario getFuncionarioSelecionado() {
		return funcionarioSelecionado;
	}

	public void setFuncionarioSelecionado(Funcionario funcionarioSelecionado) {
		this.funcionarioSelecionado = funcionarioSelecionado;
	}

	

	public Matricula getMatriculaCadastro() {
		return matriculaCadastro;
	}

	public void setMatriculaCadastro(Matricula matriculaCadastro) {
		this.matriculaCadastro = matriculaCadastro;
	}

	public String getCpfBusca() {
		return cpfBusca;
	}

	public void setCpfBusca(String cpfBusca) {
		this.cpfBusca = cpfBusca;
	}

	public List<Cargo> getCargos() {
		return cargos;
	}

	public void setCargos(List<Cargo> cargos) {
		this.cargos = cargos;
	}

	public Cargo getCargoSelecionado() {
		return cargoSelecionado;
	}

	public void setCargoSelecionado(Cargo cargoSelecionado) {
		this.cargoSelecionado = cargoSelecionado;
	}
	
	
	
}
