package br.com.ma.secretaria.Bean;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.tipotitularidadeEnum.TipoTitularidadeEnum;
import br.com.secretaria.titularidade.Titularidade;
import br.com.secretaria.titularidade.TitularidadeFacadeBean;

@ManagedBean
@ViewScoped
public class TitularidadeBean {

	private List<TipoTitularidadeEnum> tiposTitularidade;
	private Titularidade titularidadeCadastro;
	private List<Titularidade> titularidades;
	private String titulo;
	private boolean btnSalvar;
	private boolean btnEditar;

	@EJB
	private TitularidadeFacadeBean titularidadeFacadeBean;

	@PostConstruct
	public void Init() {
		TipoTitularidadeEnum[] verto = TipoTitularidadeEnum.values();
		tiposTitularidade = Arrays.asList(verto);
		titularidadeCadastro = new Titularidade();
		titularidades = titularidadeFacadeBean.findAll();
	}

	public void novo() {
		titularidadeCadastro = new Titularidade();
		visibleSalvar();

	}

	public void salvar() {
		try {

			titularidadeFacadeBean.salvar(titularidadeCadastro);
			titularidades = titularidadeFacadeBean.findAll();
			FacesUtil.addMsgInfor("Titularidadede " + titularidadeCadastro.getDescricao() + " salvo com sucesso!");
			novo();

		} catch (Exception e) {
			FacesUtil.addMsgERROR("Erro ao salvar Titularidade :" + e.getMessage());
		}
	}

	public void excluir(ActionEvent e) {

		try {
			Titularidade titularidade = (Titularidade) e.getComponent().getAttributes().get("tituSelecionado");
			System.out.println(titularidade);
			titularidadeFacadeBean.excluir(titularidade.getTitularidadeId());
			titularidades = titularidadeFacadeBean.findAll();
			FacesUtil.addMsgInfor("Titularidadede " + titularidade.getDescricao() + " removido com sucesso!");
		} catch (Exception ex) {
			FacesUtil.addMsgERROR("Erro ao excluir Titularidade :" + ex.getMessage());
		}
	}

	public void seleciona(ActionEvent e) {
		titularidadeCadastro = (Titularidade) e.getComponent().getAttributes().get("tituSelecionado");
		visibleEditar();
	}

	public void editar() {
		try {
			titularidadeFacadeBean.editar(titularidadeCadastro);
			FacesUtil.addMsgInfor("Edi��o realizada com sucesso!");

		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.addMsgERROR("Erro ao editar Titularidade :" + e.getMessage());

		}
	}

	private void visibleSalvar() {
		titulo = "Cadatro de Titularidade";
		btnEditar = false;
		btnSalvar = true;
	}

	
	
	private void visibleEditar() {
		titulo = "Atualiza��o de Titularidade";
		btnEditar = true;
		btnSalvar = false;
	}
	
	

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isBtnSalvar() {
		return btnSalvar;
	}

	public void setBtnSalvar(boolean btnSalvar) {
		this.btnSalvar = btnSalvar;
	}

	public boolean isBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(boolean btnEditar) {
		this.btnEditar = btnEditar;
	}

	public List<Titularidade> getTitularidades() {
		return titularidades;
	}

	public void setTitularidades(List<Titularidade> titularidades) {
		this.titularidades = titularidades;
	}

	public Titularidade getTitularidadeCadastro() {
		return titularidadeCadastro;
	}

	public void setTitularidadeCadastro(Titularidade titularidadeCadastro) {
		this.titularidadeCadastro = titularidadeCadastro;
	}

	public List<TipoTitularidadeEnum> getTiposTitularidade() {
		return tiposTitularidade;
	}

	public void setTiposTitularidade(List<TipoTitularidadeEnum> tiposTitularidade) {
		this.tiposTitularidade = tiposTitularidade;
	}

}
