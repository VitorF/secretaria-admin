package br.com.ma.secretaria.Bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.ma.secretaria.util.FacesUtil;
import br.com.secretaria.titularidade.Titularidade;
import br.com.secretaria.unidade.Unidade;
import br.com.secretaria.unidade.UnidadeFacadeBean;

@ManagedBean
@ViewScoped
public class UnidadeBean {
	@EJB
	private UnidadeFacadeBean unidadeFacadeBean;
	private List<Unidade> unidades;
	private Unidade unidadeCadastro;
	private boolean btnSalvar;
	private boolean btnEditar;
    private Unidade previlUnidade;
	
	
	@PostConstruct
	public void init() {
      novo();
      unidades =  unidadeFacadeBean.findAll();
	}
	
	public void novo() {
		unidadeCadastro  =  new Unidade();
		salvarDados();
	}

	public void salvar() {
		try {
            
			unidadeCadastro.setDataCadastro(new Date());
			unidadeFacadeBean.salvar(unidadeCadastro);
			unidades = unidadeFacadeBean.findAll();
			FacesUtil.addMsgInfor("Unidade " + unidadeCadastro.getDescricao() + " salvo com sucesso!");
			novo();

		} catch (Exception e) {
			FacesUtil.addMsgERROR("Erro ao salvar Titularidade :" + e.getMessage());
		}
	}

	public void excluir(ActionEvent e) {
		try {
			Unidade unidade = (Unidade) e.getComponent().getAttributes().get("unidaSelecionado");
			unidadeFacadeBean.excluir(unidade.getUnidadeId());
			unidades = unidadeFacadeBean.findAll();
			FacesUtil.addMsgInfor("Unidade " + unidade.getDescricao() + " removido com sucesso!");
		} catch (Exception ex) {
			FacesUtil.addMsgERROR("Erro ao excluir Unidade :" + ex.getMessage());
		}
	}
	
	
	public void editar() {
		try {
			unidadeFacadeBean.alterar(unidadeCadastro);
			FacesUtil.addMsgInfor("Edi��o realizada com sucesso!");

		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.addMsgERROR("Erro ao editar Titularidade :" + e.getMessage());

		}
	}

	public void selecionaUnidade(ActionEvent e) {
		unidadeCadastro =  (Unidade) e.getComponent().getAttributes().get("unidaSelecionado");
      editarDados();
	}
	
	public void salvarDados() {
		this.btnSalvar = true;
		this.btnEditar = false;
	}
	
	public void editarDados() {
		this.btnSalvar = false;
		this.btnEditar = true;
	}

	
	
	
	
	
	public Unidade getPrevilUnidade() {
		return previlUnidade;
	}

	public void setPrevilUnidade(Unidade previlUnidade) {
		this.previlUnidade = previlUnidade;
	}

	public boolean isBtnSalvar() {
		return btnSalvar;
	}

	public void setBtnSalvar(boolean btnSalvar) {
		this.btnSalvar = btnSalvar;
	}

	public boolean isBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(boolean btnEditar) {
		this.btnEditar = btnEditar;
	}

	public List<Unidade> getUnidades() {
		return unidades;
	}

	public void setUnidades(List<Unidade> unidades) {
		this.unidades = unidades;
	}

	public Unidade getUnidadeCadastro() {
		return unidadeCadastro;
	}

	public void setUnidadeCadastro(Unidade unidadeCadastro) {
		this.unidadeCadastro = unidadeCadastro;
	}

}
