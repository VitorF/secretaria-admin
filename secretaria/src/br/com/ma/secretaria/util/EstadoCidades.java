package br.com.ma.secretaria.util;

import java.util.Collection;

public class EstadoCidades {

	private static final long serialVersionUID = 1L;

	private String sigla;
	
	private String nome;
	
	private Collection<String> cidades;
	
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Collection<String> getCidades() {
		return cidades;
	}
	public void setCidades(Collection<String> cidades) {
		this.cidades = cidades;
	}
	
@Override
public String toString() {
	return "Cidades :"+this.cidades+" Estado: "+this.nome;
}
	
}
