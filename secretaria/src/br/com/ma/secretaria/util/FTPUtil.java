package br.com.ma.secretaria.util;

import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class FTPUtil {
	private static final String FTP_URL = "localhost";

	public void upalodArquivo(String pasta, InputStream arquivo, String nome) throws Exception {
		FTPClient ftp = new FTPClient();
		try {

			ftp.connect(FTPUtil.FTP_URL);
			ftp.login("educacao", "educacao");
			ftp.enterLocalPassiveMode();

			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			if (!ftp.changeWorkingDirectory(pasta)) {
				ftp.makeDirectory(pasta);
				ftp.changeWorkingDirectory(pasta);
			}
			System.out.println("Diretorio Atual : " + ftp.printWorkingDirectory());
			if (ftp.storeFile(nome, arquivo)) {
				System.out.println("Upaload Efetuado com sucesso");
			} else {
				System.err.println("Erro ao efetuar o upaload");
				throw new IllegalArgumentException(" Erro ao Efetuar Upload");
			}
			arquivo.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (ftp.isConnected()) {
				ftp.logout();
				ftp.disconnect();
			}
		}
	}

	public InputStream buscaArquivo(String pasta, String nome) throws Exception {
		FTPClient ftp = new FTPClient();
		InputStream inputStream = null;
		try {

			ftp.connect(FTPUtil.FTP_URL);
			ftp.login("educacao", "educacao");
			ftp.enterLocalPassiveMode();

			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			if (!ftp.changeWorkingDirectory(pasta)) {
				ftp.makeDirectory(pasta);
				ftp.changeWorkingDirectory(pasta);
			}
			System.out.println("Diretorio Atual : " + ftp.printWorkingDirectory());

			inputStream = ftp.retrieveFileStream(nome);

			System.out.println("achou");
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (ftp.isConnected()) {
				ftp.logout();
				ftp.disconnect();
			}

		}

		return inputStream;
	}

	public void excluirArquivo(String pasta, String nome) throws Exception {
		FTPClient ftp = new FTPClient();
		try {

			ftp.connect(FTPUtil.FTP_URL);
			ftp.login("educacao", "educacao");
			ftp.enterLocalPassiveMode();
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.changeWorkingDirectory(pasta);
			System.out.println("Diretorio Atual : " + ftp.printWorkingDirectory());
			if (ftp.deleteFile(nome)) {
				System.out.println("Exclus�o FTP Efetuada  com sucesso");
			} else {
				System.err.println("Erro ao efetuar a Exclus�o");
				throw new IllegalArgumentException(" Erro ao Efetuar a Exclus�o");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (ftp.isConnected()) {
				ftp.logout();
				ftp.disconnect();
			}
		}
	}

}
