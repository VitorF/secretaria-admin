package br.com.ma.secretaria.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class FacesUtil {


	public static void addMsgInfor(String messagem) {
		FacesMessage fc  =  new FacesMessage(FacesMessage.SEVERITY_INFO, messagem, messagem);
		FacesContext.getCurrentInstance().addMessage(null, fc);
	}
	
	public static void addMsgERROR(String mensagem) {
		FacesMessage fc  =  new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, mensagem);
		FacesContext.getCurrentInstance().addMessage(null, fc);

	}
	
	public static void addMsgWARN(String mensagem) {
		FacesMessage fc =  new FacesMessage(FacesMessage.SEVERITY_WARN, mensagem, mensagem);
		FacesContext.getCurrentInstance().addMessage(null, fc);
	}
	
	public static String getParam(String parametro) {
		
		FacesContext context =   FacesContext.getCurrentInstance();
		ExternalContext externalContext =  context.getExternalContext();
		String valor  = externalContext.getRequestParameterMap().get(parametro);
		return valor;
	}
	
	
}
